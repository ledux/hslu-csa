﻿//------------------------------------------------------------------------------
// C #   I N   A C T I O N   ( C S A )
//------------------------------------------------------------------------------
// Repository:
//    $Id: Robot.cs 1039 2016-10-25 11:56:45Z chj-hslu $
//------------------------------------------------------------------------------
using System;

namespace RobotCtrl
{
    public class Robot : IDisposable
    {

        #region members
        private readonly RobotConsole _robotConsole;
        private readonly Radar _radar;
        private readonly Drive _drive;
        #endregion


        #region constructor & destructor
        public Robot()
        {
            _robotConsole = new RobotConsole();
            _radar = new Radar(Constants.IORadarSensor);
            _drive = new Drive();
        }

        /// <summary>
        /// Disposed das Drive-Objekt sowie die robotConsole.
        /// Damit werden die Worker-Threads in diesen Objekten beendet
        /// </summary>
        public void Dispose()
        {
            _drive.Dispose();
            _robotConsole.Dispose();
        }
        #endregion


        #region properties
        /// <summary>
        /// Liefert die Konsole des Roboters
        /// </summary>
        public RobotConsole RobotConsole
        {
            get { return _robotConsole; }
        }


        /// <summary>
        /// Liefert den Radar des Roboters
        /// </summary>
        public Radar Radar
        {
            get { return _radar; }
        }


        /// <summary>
        /// Liefert den Antrieb des Roboters
        /// </summary>
        public Drive Drive
        {
            get { return _drive; }
        }


        /// <summary>
        /// Liefert bzw. setzt die aktuelle Postion des Roboters.
        /// </summary>
        public PositionInfo Position { get { return _drive.Position; } set { _drive.Position = value; } }
        #endregion
    }
}
