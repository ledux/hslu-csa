﻿using System;
using System.Threading;
using RobotCtrl;

namespace RobotControl
{
    public class RadarSuperVisor
    {
        private EventHandler _tooCloseEventHandler;
        private readonly Radar _radar;

        public RadarSuperVisor(Radar radar)
        {
            _radar = radar;
        }

        public event EventHandler TooClose
        {
            add { _tooCloseEventHandler += value; }
            remove { _tooCloseEventHandler -= value; }
        }

        public void NotifyIfTooClose(object minDistance)
        {
            float distance = minDistance as float? ?? 20;
            while (true)
            {
                var distanceInCm = _radar.Distance*100;
                if (distanceInCm < distance)
                {
                    _tooCloseEventHandler.Invoke(this, EventArgs.Empty);
                }
                Thread.Sleep(50);
            }
        }
    }
}
