﻿using System.Windows.Forms;
using RobotCtrl;

namespace TestConsole
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            var console = new RobotConsole();
            _consoleView.Console = console;
        }
    }
}
