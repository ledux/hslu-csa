﻿using System;
using System.Windows.Forms;

namespace TestConsole
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Application.Run(new Form1());
        }
    }
}
