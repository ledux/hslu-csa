﻿using RobotCtrl;

namespace TestMotor_
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;
        private RobotView.DriveCtrlView _driveControlView;
        private RobotView.MotorCtrlView _rightCtrlView;
        private RobotView.MotorCtrlView _leftCtrlView;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this._driveControlView = new RobotView.DriveCtrlView();
            this._leftCtrlView = new RobotView.MotorCtrlView();
            this._rightCtrlView = new RobotView.MotorCtrlView();
            this.SuspendLayout();
            // 
            // _driveControlView
            // 
            this._driveControlView.DriveCtrl = new DriveCtrl(Constants.IODriveCtrl);
            this._driveControlView.Location = new System.Drawing.Point(0, 0);
            this._driveControlView.Name = "_driveControlView";
            this._driveControlView.Size = new System.Drawing.Size(274, 57);
            this._driveControlView.TabIndex = 0;
            // 
            // _leftCtrlView
            // 
            this._leftCtrlView.Location = new System.Drawing.Point(0, 73);
            this._leftCtrlView.MotorCtrl = new MotorCtrl(Constants.IOMotorCtrlLeft);
            this._leftCtrlView.Name = "_leftCtrlView";
            this._leftCtrlView.Size = new System.Drawing.Size(431, 258);
            this._leftCtrlView.TabIndex = 1;
            // 
            // _rightCtrlView
            // 
            this._rightCtrlView.Location = new System.Drawing.Point(426, 73);
            this._rightCtrlView.MotorCtrl = new MotorCtrl(Constants.IOMotorCtrlRight);
            this._rightCtrlView.Name = "_rightCtrlView";
            this._rightCtrlView.Size = new System.Drawing.Size(431, 258);
            this._rightCtrlView.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(859, 455);
            this.Controls.Add(this._driveControlView);
            this.Controls.Add(this._leftCtrlView);
            this.Controls.Add(this._rightCtrlView);
            this.Menu = this.mainMenu1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion
    }
}

