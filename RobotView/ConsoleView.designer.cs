namespace RobotView
{
    partial class ConsoleView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        private LedView ledView1;
        private LedView ledView2;
        private LedView ledView3;
        private LedView ledView4;
        private SwitchView switchView1;
        private SwitchView switchView2;
        private SwitchView switchView3;
        private SwitchView switchView4;
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ledView1 = new RobotView.LedView();
            this.ledView2 = new RobotView.LedView();
            this.ledView3 = new RobotView.LedView();
            this.ledView4 = new RobotView.LedView();
            this.switchView1 = new RobotView.SwitchView();
            this.switchView2 = new RobotView.SwitchView();
            this.switchView3 = new RobotView.SwitchView();
            this.switchView4 = new RobotView.SwitchView();
            this.lblLed1 = new System.Windows.Forms.Label();
            this.lblLed2 = new System.Windows.Forms.Label();
            this.lblLed3 = new System.Windows.Forms.Label();
            this.lblLed4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ledView1
            // 
            this.ledView1.Led = null;
            this.ledView1.Location = new System.Drawing.Point(58, 0);
            this.ledView1.Name = "ledView1";
            this.ledView1.Size = new System.Drawing.Size(20, 20);
            this.ledView1.State = false;
            this.ledView1.TabIndex = 0;
            // 
            // ledView2
            // 
            this.ledView2.Led = null;
            this.ledView2.Location = new System.Drawing.Point(38, 0);
            this.ledView2.Name = "ledView2";
            this.ledView2.Size = new System.Drawing.Size(20, 20);
            this.ledView2.State = false;
            this.ledView2.TabIndex = 1;
            // 
            // ledView3
            // 
            this.ledView3.Led = null;
            this.ledView3.Location = new System.Drawing.Point(18, 0);
            this.ledView3.Name = "ledView3";
            this.ledView3.Size = new System.Drawing.Size(20, 20);
            this.ledView3.State = false;
            this.ledView3.TabIndex = 2;
            // 
            // ledView4
            // 
            this.ledView4.Led = null;
            this.ledView4.Location = new System.Drawing.Point(0, 0);
            this.ledView4.Name = "ledView4";
            this.ledView4.Size = new System.Drawing.Size(20, 20);
            this.ledView4.State = false;
            this.ledView4.TabIndex = 3;
            // 
            // switchView1
            // 
            this.switchView1.Location = new System.Drawing.Point(78, 0);
            this.switchView1.Name = "switchView1";
            this.switchView1.Size = new System.Drawing.Size(20, 40);
            this.switchView1.State = false;
            this.switchView1.Switch = null;
            this.switchView1.TabIndex = 4;
            // 
            // switchView2
            // 
            this.switchView2.Location = new System.Drawing.Point(98, 0);
            this.switchView2.Name = "switchView2";
            this.switchView2.Size = new System.Drawing.Size(20, 40);
            this.switchView2.State = false;
            this.switchView2.Switch = null;
            this.switchView2.TabIndex = 5;
            // 
            // switchView3
            // 
            this.switchView3.Location = new System.Drawing.Point(118, 0);
            this.switchView3.Name = "switchView3";
            this.switchView3.Size = new System.Drawing.Size(20, 40);
            this.switchView3.State = false;
            this.switchView3.Switch = null;
            this.switchView3.TabIndex = 6;
            // 
            // switchView4
            // 
            this.switchView4.Location = new System.Drawing.Point(137, 0);
            this.switchView4.Name = "switchView4";
            this.switchView4.Size = new System.Drawing.Size(20, 40);
            this.switchView4.State = false;
            this.switchView4.Switch = null;
            this.switchView4.TabIndex = 7;
            // 
            // lblLed1
            // 
            this.lblLed1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblLed1.Location = new System.Drawing.Point(0, 23);
            this.lblLed1.Name = "lblLed1";
            this.lblLed1.Size = new System.Drawing.Size(20, 20);
            this.lblLed1.Text = "1";
            this.lblLed1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblLed2
            // 
            this.lblLed2.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblLed2.Location = new System.Drawing.Point(18, 23);
            this.lblLed2.Name = "lblLed2";
            this.lblLed2.Size = new System.Drawing.Size(20, 20);
            this.lblLed2.Text = "2";
            this.lblLed2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblLed3
            // 
            this.lblLed3.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblLed3.Location = new System.Drawing.Point(38, 23);
            this.lblLed3.Name = "lblLed3";
            this.lblLed3.Size = new System.Drawing.Size(20, 20);
            this.lblLed3.Text = "3";
            this.lblLed3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblLed4
            // 
            this.lblLed4.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblLed4.Location = new System.Drawing.Point(58, 23);
            this.lblLed4.Name = "lblLed4";
            this.lblLed4.Size = new System.Drawing.Size(20, 20);
            this.lblLed4.Text = "4";
            this.lblLed4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // ConsoleView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.lblLed4);
            this.Controls.Add(this.lblLed3);
            this.Controls.Add(this.lblLed2);
            this.Controls.Add(this.lblLed1);
            this.Controls.Add(this.ledView1);
            this.Controls.Add(this.ledView2);
            this.Controls.Add(this.ledView3);
            this.Controls.Add(this.ledView4);
            this.Controls.Add(this.switchView1);
            this.Controls.Add(this.switchView2);
            this.Controls.Add(this.switchView3);
            this.Controls.Add(this.switchView4);
            this.Name = "ConsoleView";
            this.Size = new System.Drawing.Size(162, 46);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblLed1;
        private System.Windows.Forms.Label lblLed2;
        private System.Windows.Forms.Label lblLed3;
        private System.Windows.Forms.Label lblLed4;
    }
}
