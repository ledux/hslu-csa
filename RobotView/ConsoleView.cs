using System;
using System.Windows.Forms;
using RobotCtrl;

namespace RobotView
{
    public partial class ConsoleView : UserControl
    {
        public event EventHandler led1Handler;
        public event EventHandler led2Handler;
        public event EventHandler led3Handler;
        public event EventHandler led4Handler;
        public event EventHandler switch1Handler;
        public event EventHandler switch2Handler;
        public event EventHandler switch3Handler;
        public event EventHandler switch4Handler;

        public ConsoleView()
        {
            InitializeComponent();
        }

        public RobotConsole Console { get; set; }

        public void SetLeds(bool on)
        {
            ledView1.State = on;
            ledView2.State = on;
            ledView3.State = on;
            ledView4.State = on;
        }

        public void SetSwitch1(bool up)
        {
            switchView1.State = up;
        }
        public void SetSwitch2(bool up)
        {
            switchView2.State = up;
        }
        public void SetSwitch3(bool up)
        {
            switchView3.State = up;
        }
        public void SetSwitch4(bool up)
        {
            switchView4.State = up;
        }
    }
}