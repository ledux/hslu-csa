using System.Windows.Forms;
using RobotCtrl;

namespace RobotView
{
    public partial class SwitchView : UserControl
    {
        #region fields
        private Switch _switch;
        private bool _state;
        #endregion

        #region constructors
        /// <summary>
        /// Default Constructor
        /// </summary>
        public SwitchView()
        {
            InitializeComponent();
        }
        #endregion

        #region properties
        /// <summary>
        /// If the switch is on or off
        /// </summary>
        public bool State
        {
            get { return _state; }
            set
            {
                pbSwitch.Image = value ? Resources.Resources.SwitchOn : Resources.Resources.SwitchOff;
                _state = value;
            }
        }

        /// <summary>
        /// The switch with the functionality
        /// </summary>
        public Switch Switch
        {
            get { return _switch; }
            set
            {
                if (value != null)
                {
                    value.SwitchStateChanged += OnSwitchChangedEvent;
                }
                _switch = value;
            }
        }
        #endregion

        #region private methods
        private void OnSwitchChangedEvent(object sender, SwitchEventArgs args)
        {
            State = !State;
        }
        #endregion

        private void pbSwitch_Click(object sender, System.EventArgs e)
        {

        }
    }
}