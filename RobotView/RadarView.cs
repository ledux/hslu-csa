using System;
using System.Globalization;
using System.Windows.Forms;
using RobotCtrl;

namespace RobotView
{
    public partial class RadarView : UserControl
    {
        public RadarView()
        {
            InitializeComponent();
            timer1.Enabled = true;
        }

        public Radar Radar { get; set; }

        private void timer1_Tick(object sender, System.EventArgs e)
        {
            if (Radar != null)
            {
                float radarDistance = Radar.Distance;
                txt_distance.Text = radarDistance.ToString(CultureInfo.InvariantCulture);
            }
        }
    }
}