using System.Windows.Forms;
using RobotCtrl;

namespace RobotView
{
    public partial class LedView : UserControl
    {
        #region fields
        private Led _led;
        private bool _state;
        #endregion

        #region constructors
        /// <summary>
        /// Default Constructor
        /// </summary>
        public LedView()
        {
            InitializeComponent();
        }
        #endregion

        #region properties
        /// <summary>
        /// If a led is on or off
        /// </summary>
        public bool State
        {
            get { return _state; }
            set
            {
                pbLed.Image = value ? Resources.Resources.LedOn : Resources.Resources.LedOff;
                _state = value;
            }
        }

        /// <summary>
        /// The Led
        /// </summary>
        public Led Led
        {
            get { return _led; }
            set
            {
                _led = value;
                if (value != null)
                {
                    value.LedStateChanged += OnLedChangedEvent;
                }
            }
        }
        #endregion

        #region private methods
        private void OnLedChangedEvent(object sender, LedEventArgs args)
        {
            State = !State;
        }
        #endregion
    }
}