﻿using RobotView;

namespace TestDrive
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private DriveView _driveView;
        private CommonRunParameters _commonRunParameters;
        private RunLine _runLine;
        private RunTurn _runTurn;
        private RunArc _runArc;
        private RadarView _radarView;
        //private RunArc _runArcRight;
        private ConsoleView _consoleView;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._driveView = new RobotView.DriveView();
            this._commonRunParameters = new RobotView.CommonRunParameters();
            this._runLine = new RobotView.RunLine();
            this._runTurn = new RobotView.RunTurn();
            this._runArc = new RobotView.RunArc();
            this._radarView = new RobotView.RadarView();
            this._consoleView = new RobotView.ConsoleView();
            this.SuspendLayout();
            // 
            // _driveView
            // 
            this._driveView.Drive = null;
            this._driveView.Location = new System.Drawing.Point(0, 145);
            this._driveView.Name = "_driveView";
            this._driveView.Size = new System.Drawing.Size(292, 289);
            this._driveView.TabIndex = 0;
            // 
            // _commonRunParameters
            // 
            this._commonRunParameters.Acceleration = 0.3F;
            this._commonRunParameters.Location = new System.Drawing.Point(303, 3);
            this._commonRunParameters.Name = "_commonRunParameters";
            this._commonRunParameters.Size = new System.Drawing.Size(401, 84);
            this._commonRunParameters.Speed = 0.5F;
            this._commonRunParameters.TabIndex = 1;
            // 
            // _runLine
            // 
            this._runLine.Acceleration = 0F;
            this._runLine.Drive = null;
            this._runLine.Length = 1F;
            this._runLine.Location = new System.Drawing.Point(303, 93);
            this._runLine.Name = "_runLine";
            this._runLine.Size = new System.Drawing.Size(412, 46);
            this._runLine.Speed = 0F;
            this._runLine.TabIndex = 2;
            // 
            // _runTurn
            // 
            this._runTurn.Acceleration = 0F;
            this._runTurn.Drive = null;
            this._runTurn.Location = new System.Drawing.Point(301, 145);
            this._runTurn.Name = "_runTurn";
            this._runTurn.Size = new System.Drawing.Size(414, 45);
            this._runTurn.Speed = 0F;
            this._runTurn.TabIndex = 3;
            // 
            // _runArc
            // 
            this._runArc.Acceleration = 0F;
            this._runArc.Drive = null;
            this._runArc.Location = new System.Drawing.Point(303, 196);
            this._runArc.Name = "_runArc";
            this._runArc.Size = new System.Drawing.Size(412, 90);
            this._runArc.Speed = 0F;
            this._runArc.TabIndex = 4;
            // 
            // _radarView
            // 
            this._radarView.Location = new System.Drawing.Point(3, 3);
            this._radarView.Name = "_radarView";
            this._radarView.Radar = null;
            this._radarView.Size = new System.Drawing.Size(297, 106);
            this._radarView.TabIndex = 5;
            // 
            // _consoleView
            // 
            this._consoleView.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this._consoleView.Console = null;
            this._consoleView.Location = new System.Drawing.Point(359, 379);
            this._consoleView.Name = "_consoleView";
            this._consoleView.Size = new System.Drawing.Size(153, 55);
            this._consoleView.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(788, 455);
            this.Controls.Add(this._driveView);
            this.Controls.Add(this._commonRunParameters);
            this.Controls.Add(this._runLine);
            this.Controls.Add(this._runTurn);
            this.Controls.Add(this._runArc);
            this.Controls.Add(this._radarView);
            this.Controls.Add(this._consoleView);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion
    }
}

