﻿using System;
using System.Threading;
using System.Windows.Forms;
using RobotControl;
using RobotCtrl;

namespace TestDrive
{
    public partial class Form1 : Form
    {
        private readonly Robot _robot;
        private Thread _blinkThread;
        private bool _blinking;

        public Form1()
        {
            InitializeComponent();
            _robot = new Robot();
            _robot.Drive.Power = true;
            _driveView.Drive = _robot.Drive;
            _runArc.Drive = _robot.Drive;
            _runLine.Drive = _robot.Drive;
            _runTurn.Drive = _robot.Drive;
            _radarView.Radar = _robot.Radar;

            _robot.Drive.StartDriveEventHandler += StopBlinking;

            var radarSupervisor = new RadarSuperVisor(_robot.Radar);
            radarSupervisor.TooClose += StopRoboter;
            radarSupervisor.TooClose += StartBlinking;
            var radarThread = new Thread(radarSupervisor.NotifyIfTooClose);
            radarThread.Start(30f);

            _consoleView.Console = _robot.RobotConsole;


            SetInitValues();

            (_robot.RobotConsole[Switches.Switch1]).SwitchStateChanged += StartRunLine;
            (_robot.RobotConsole[Switches.Switch2]).SwitchStateChanged += StartRunTurn;
            (_robot.RobotConsole[Switches.Switch3]).SwitchStateChanged += StartRunArc;
            (_robot.RobotConsole[Switches.Switch1]).SwitchStateChanged += ToggleSwitch1;
            (_robot.RobotConsole[Switches.Switch2]).SwitchStateChanged += ToggleSwitch2;
            (_robot.RobotConsole[Switches.Switch3]).SwitchStateChanged += ToggleSwitch3;

            _robot.RobotConsole[Leds.Led1].LedStateChanged += ToggleSoftwareLed;
            _robot.RobotConsole[Leds.Led2].LedStateChanged += ToggleSoftwareLed;
            _robot.RobotConsole[Leds.Led3].LedStateChanged += ToggleSoftwareLed;
            _robot.RobotConsole[Leds.Led4].LedStateChanged += ToggleSoftwareLed;

        }

        private void ToggleSwitch1(object sender, SwitchEventArgs e)
        {
            if (_consoleView.InvokeRequired)
            {
                _consoleView.Invoke((Action)(() => _consoleView.SetSwitch1(e.SwitchEnabled)));
            }
        }
        private void ToggleSwitch2(object sender, SwitchEventArgs e)
        {
            if (_consoleView.InvokeRequired)
            {
                _consoleView.Invoke((Action)(() => _consoleView.SetSwitch2(e.SwitchEnabled)));
            }
        }
        private void ToggleSwitch3(object sender, SwitchEventArgs e)
        {
            if (_consoleView.InvokeRequired)
            {
                _consoleView.Invoke((Action)(() => _consoleView.SetSwitch3(e.SwitchEnabled)));
            }
        }

        private void ToggleSoftwareLed(object sender, LedEventArgs ledEventArgs)
        {
            if (_consoleView.InvokeRequired)
            {
                _consoleView.Invoke((Action)(() => _consoleView.SetLeds(ledEventArgs.LedEnabled)));
            }
        }

        private void StartRunLine(object sender, EventArgs e)
        {
            if (_runLine.InvokeRequired)
            {
                _runLine.Invoke((Action)(() => _runLine.Start()));
            }
        }

        private void StartRunTurn(object sender, EventArgs e)
        {
            if (_runTurn.InvokeRequired)
            {
                _runTurn.Invoke((Action)(() => _runTurn.Start()));
            }
        }

        private void StartRunArc(object sender, EventArgs e)
        {
            if (_runArc.InvokeRequired)
            {
                _runArc.Invoke((Action)(() => _runArc.Start()));
            }
        }



        private void StopBlinking(object sender, EventArgs e)
        {
            if (_blinking)
            {
                _blinking = false;
                _blinkThread.Abort();
                _robot.RobotConsole[Leds.Led1].LedEnabled = false;
                _robot.RobotConsole[Leds.Led2].LedEnabled = false;
                _robot.RobotConsole[Leds.Led3].LedEnabled = false;
                _robot.RobotConsole[Leds.Led4].LedEnabled = false;
            }
        }


        private void StartBlinking(object sender, EventArgs e)
        {
            if (!_blinking)
            {
                _blinking = true;
                _blinkThread = new Thread(BlinkLed);
                _blinkThread.Start();
            }
        }

        private void BlinkLed()
        {
            while (true)
            {

                try
                {
                    _robot.RobotConsole[Leds.Led1].LedEnabled = !_robot.RobotConsole[Leds.Led1].LedEnabled;
                    _robot.RobotConsole[Leds.Led2].LedEnabled = !_robot.RobotConsole[Leds.Led2].LedEnabled;
                    _robot.RobotConsole[Leds.Led3].LedEnabled = !_robot.RobotConsole[Leds.Led3].LedEnabled;
                    _robot.RobotConsole[Leds.Led4].LedEnabled = !_robot.RobotConsole[Leds.Led4].LedEnabled;

                    Thread.Sleep(700);

                }
                catch (ThreadAbortException)
                {
                    break;
                }
            }
        }

        private void StopRoboter(object sender, EventArgs eventArgs)
        {
            _robot.Drive.Stop();
        }


        private void SetInitValues()
        {
            _runLine.Speed = _commonRunParameters.Speed;
            _runLine.Acceleration = _commonRunParameters.Acceleration;
            _runArc.Speed = _commonRunParameters.Speed;
            _runArc.Acceleration = _commonRunParameters.Acceleration;
            _runTurn.Speed = _commonRunParameters.Speed;
            _runTurn.Acceleration = _commonRunParameters.Acceleration;
        }

        private void CommonRunParametersOnAccelerationChanged(object sender, EventArgs eventArgs)
        {
            _runLine.Acceleration = _commonRunParameters.Acceleration;
            _runArc.Acceleration = _commonRunParameters.Acceleration;
            _runTurn.Acceleration = _commonRunParameters.Acceleration;
        }

        private void CommonRunParametersOnSpeedChanged(object sender, EventArgs eventArgs)
        {
            _runLine.Speed = _commonRunParameters.Speed;
            _runArc.Speed = _commonRunParameters.Speed;
            _runTurn.Speed = _commonRunParameters.Speed;
        }
    }
}
